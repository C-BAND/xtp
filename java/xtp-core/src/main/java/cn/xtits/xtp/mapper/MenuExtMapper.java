package cn.xtits.xtp.mapper;

import cn.xtits.xtp.dto.MenuWithOperationDto;
import cn.xtits.xtp.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuExtMapper {

    List<Menu> listMenuByUserId(@Param("appId") Integer appId, @Param("userId") Integer userId);

    List<MenuWithOperationDto> listMenuWithOperationByUserId(@Param("appId") Integer appId, @Param("userId") Integer userId);

    List<Menu> listMenuUserFavorite(@Param("userId") Integer userId);

}
