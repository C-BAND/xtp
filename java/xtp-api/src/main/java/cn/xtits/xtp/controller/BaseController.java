package cn.xtits.xtp.controller;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

public class BaseController {


    public Integer getUserId() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return Integer.parseInt(request.getAttribute("userId").toString());
    }

    public String getUserName() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request.getAttribute("userName").toString() == null ? "test" : request.getAttribute("userName").toString();
    }

    public String getOauth() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request.getParameter("oauth") == null ? "test" : request.getParameter("oauth");
    }

    public String getAppToken() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request.getAttribute("appToken").toString();
    }

    public Date getDateNow() {
        return new Date();
    }


}
