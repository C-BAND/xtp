package cn.xtits.xtp.service;

import cn.xtits.xtp.entity.Custom;
import cn.xtits.xtp.entity.CustomExample;
import java.util.List;

/**
 * Created by 
 */
public interface CustomService {

    int deleteByPrimaryKey(Integer id);

    int insert(Custom record);

    List<Custom> listByExample(CustomExample example);

    Custom getByPrimaryKey(Integer id);

    int updateByPrimaryKey(Custom record);
    
    int updateByPrimaryKeySelective(Custom record);
}