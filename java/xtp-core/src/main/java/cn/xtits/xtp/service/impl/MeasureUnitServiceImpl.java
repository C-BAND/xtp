package cn.xtits.xtp.service.impl;
import cn.xtits.xtp.entity.MeasureUnit;
import cn.xtits.xtp.entity.MeasureUnitExample;
import cn.xtits.xtp.mapper.base.MeasureUnitMapper;
import cn.xtits.xtp.service.MeasureUnitService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dan on 2018-02-28 01:53:21
 */
@Service
public class MeasureUnitServiceImpl implements MeasureUnitService {

    @Resource
    private MeasureUnitMapper measureUnitMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return measureUnitMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(MeasureUnit record) {
        return measureUnitMapper.insertSelective(record);
    }

    @Override
    public List<MeasureUnit> listByExample(MeasureUnitExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) measureUnitMapper.selectByExample(example);
        example.setCount((int)page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public MeasureUnit getByPrimaryKey(Integer id) {
        return measureUnitMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(MeasureUnit record) {
        return measureUnitMapper.updateByPrimaryKey(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(MeasureUnit record) {
        return measureUnitMapper.updateByPrimaryKeySelective(record);
    }



}