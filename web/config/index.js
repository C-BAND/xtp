"use strict"

const base = require("./base.conf");
const webpackMerge = require("webpack-merge");

const processArgv = require("../build/process.argv.json");

console.log("processArgv", processArgv);

// 项目名称
const projectNameFun = () => {
	const argvArguments = processArgv || [];
	const indexArgv = argvArguments.indexOf("--project");
	let projectName = null;
	if (indexArgv > 0) {
		projectName = argvArguments[indexArgv + 1];
	}
	return projectName;
};

const projectName = projectNameFun();

const config = webpackMerge(base, {
	build: {
		assetsPublicPath: `/${projectName}/` || '/'
	},
	dev: {
		assetsSubDirectory: "static",
		assetsPublicPath: '/',
	}
});

module.exports = config;