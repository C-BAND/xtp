
package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.AppFavorite;
import cn.xtits.xtp.entity.AppFavoriteExample;
import cn.xtits.xtp.mapper.base.AppFavoriteMapper;
import cn.xtits.xtp.service.AppFavoriteService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ShengHaiJiang on 2017/3/7.
 */
@Service
public class AppFavoriteServiceImpl implements AppFavoriteService {

    @Resource
    private AppFavoriteMapper appFavoriteMapper;


    @Override
    public int deleteByPrimaryKey(Integer ID) {
        return appFavoriteMapper.deleteByPrimaryKey(ID);
    }

    @Override
    public int insert(AppFavorite record) {
        return appFavoriteMapper.insert(record);
    }

    @Override
    public List<AppFavorite> listByExample(AppFavoriteExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) appFavoriteMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public AppFavorite getByPrimaryKey(Integer ID) {
        return appFavoriteMapper.selectByPrimaryKey(ID);
    }

    @Override
    public int updateByPrimaryKey(AppFavorite record) {
        return appFavoriteMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateAppFavorite(Integer appId, String menuIds) {
        AppFavoriteExample example = new AppFavoriteExample();
        AppFavoriteExample.Criteria criteria = example.createCriteria();
        criteria.andAppIdEqualTo(appId);
        List<AppFavorite> list = appFavoriteMapper.selectByExample(example);
        for (AppFavorite appFavorite : list) {
            appFavoriteMapper.deleteByPrimaryKey(appFavorite.getId());
        }
        String[] ids = menuIds.split(",");
        for (String id : ids) {
            AppFavorite roleMenu = new AppFavorite();
            roleMenu.setAppId(appId);
            roleMenu.setMenuId(Integer.parseInt(id));
            appFavoriteMapper.insert(roleMenu);
        }
        return 1;
    }
}