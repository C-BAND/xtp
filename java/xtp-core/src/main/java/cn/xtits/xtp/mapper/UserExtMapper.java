package cn.xtits.xtp.mapper;

import cn.xtits.xtp.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserExtMapper {

    User getUserByAppUserId(@Param("appId") Integer appId, @Param("appUserId") Integer appUserId);

    List<User> listUserByRoleId(@Param("roleIds") List<Integer> roleIds);
}
