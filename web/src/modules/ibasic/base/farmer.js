import { IBASIC } from "../../api";
import { Message } from "element-ui";

const Farmer = function() {
  return {
    id: 0,
    code: null,
    name: null,
    shortName: null,
    director: null,
    tel: null,
    phone: null,
    address: null,
    taxId: null,
    mail: null,
    site: null,
    fax: null,
    remark: null,
    operatorId: null,
    operatorName: null,
    salesManId: null,
    salesManName: null,
    areaId: null,
    areaCode: null,
    areaName: null,
    yearBalance: null,
    payDay: null,
    post: null,
    payMode: null
  };
},
FarmerAPI = {
  farmer: new Farmer(),
  get: () => FarmerAPI.farmer,
  set: (value) => {
    FarmerAPI.farmer = Object.assign(FarmerAPI.farmer, value);
  },
  init: () => new Farmer(),
  getFarmer(params) {
    return new Promise((resolve) => {
      IBASIC.farmer.getFarmer(params).then(({data, res}) => {
        let item = {};
        if (data.code === 1) {item = data.data;}
        resolve({ data, item, res });
        });
    });
  },
  listFarmer(params) {
    return new Promise((resolve) => {
      IBASIC.farmer.listFarmer(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        resolve({ data, list, res });
        });
    });
  },
  insertFarmer(params, showMsgFlag = true, msg) {
    return new Promise((resolve) => {
      IBASIC.farmer.insertFarmer(params).then(({data, res}) => {
        if (showMsgFlag) {
          Message({
            message: data.msg || msg || (data.code === 1 ? "新增成功" : "新增失败"),
            type: data.code === 1 ? "success" : "error"
          });
        }
        resolve({data, res});
        });
    });
  },
  updateFarmer(params, showMsgFlag = true, msg) {
    return new Promise((resolve) => {
      IBASIC.farmer.updateFarmer(params).then(({data, res}) => {
        if (showMsgFlag) {
          Message({
            message: data.msg || msg || (data.code === 1 ? "修改成功" : "修改失败"),
            type: data.code === 1 ? "success" : "error"
          });
        }
        resolve({data, res});
        });
    });
  },
  deleteFarmer(params, showMsgFlag = true, msg) {
    return new Promise((resolve) => {
      IBASIC.farmer.deleteFarmer(params).then(({data, res}) => {
        if (showMsgFlag) {
          Message({
            message: data.msg || msg || (data.code === 1 ? "删除成功" : "删除失败"),
            type: data.code === 1 ? "success" : "error"
          });
        }
        resolve({data, res});
        });
    });
  }
};

export { FarmerAPI };
