
window.TOKEN_KEY = "app-token";

window.WEBSOCKET = {
  ENABLE: false,
  URL: "/myWebSocket"
};

// xtp模块PC端
window.SYSTEM_CONFIG = {
  modules: "PC",
  homePage: {
    path: "/",
    title: "首页"
  }
};

window.THEME_CONFIG = {
  MAIN_PATH: "/",
  THEME_SWITCH: false,
  LANGUAGE_SWITCH: false,
  THEME: "default",
  SYSTEM_NAME: "xstage后台管理系统",
  SUB_NAME: "后台管理",
  NAV_BAR_NAME: "xstage后台管理系统"
};

window.PROJECT_NAME = "xtp";

window.env = {
  NODE_ENV: "development"
};

window.APP_SETTING = {
  SERVER_HOST: "119.37.194.4",
  SERVER_PORT: 15555,
  API: "xstage-api",
  XTP: {
    SERVER_HOST: "119.37.194.4",
    SERVER_PORT: 15555,
    API: "xstage-api"
  },
	IBASIC: {
		SERVER_HOST: "119.37.194.4", //"192.168.2.100",
		SERVER_PORT: 15555, //50000,
		API: "xstage-api" //"xstage-api" //ibasic-api
	}
};

window.APP_SETTING_WEB = {
  SERVER_HOST: "localhost",
  SERVER_PORT: 8002,
  IMM: {
    SERVER_HOST: "localhost",
    SERVER_PORT: 8002,
    PATH: "/xstage"
  }
};
